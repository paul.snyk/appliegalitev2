﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleSheetsToUnity;
using Random = UnityEngine.Random;
using System.IO;
using System.Text;
using UnityEngine.Networking;

public class DataBase : MonoBehaviour
{
    public GameObject cardGo;
    public SpriteRenderer imageCard;
    
    
    public string[] index;
    public string[] data;
    public string[] dataOnline;
    public List<string> listData;

    public string goodAnswers;
    public string[] goodAnswersLeftQuote;
    public string[] goodAnswersRightQuote;

    public int randomNmb;

    
    void Awake()
    {
        listData = ListData.listData;
        CalculateRandomNumber();
    }

    public void CalculateRandomNumber()
    {
        //Phrase aléatoire
        Debug.Log(listData.Count);
        randomNmb = Random.Range(0, listData.Count);

        //Séparation des colones 
        for (int i = 0; i < listData.Count; i++)
        {
            index = listData[randomNmb].Split(new char[] { '|' });
        }

        //Détection de la bonne réponse
        if (index[5].Contains("R/"))
        {
            goodAnswers = index[5];
        }
        else if (index[6].Contains("R/"))
        {
            goodAnswers = index[6];
        }
        cardGo.SetActive(true);

        //void affichage text
        ChangeInfo();
        //Affichage images
        StartCoroutine(GetTexture(index[7]));
    }

    public void ChangeInfo()
    {
        //Affichage texte
        GameObject.FindObjectOfType<Card>().dialogue = index[0];

        //Affichage réponses
        GameObject.FindObjectOfType<Card>().leftQuote = index[6];
        if (index[6].Contains("R/"))
        {
            goodAnswersLeftQuote = index[6].Split(new char[] { '/' });
            GameObject.FindObjectOfType<Card>().leftQuote = goodAnswersLeftQuote[1];
        }
        GameObject.FindObjectOfType<Card>().rightQuote = index[5];
        if (index[5].Contains("R/"))
        {
            goodAnswersRightQuote = index[5].Split(new char[] { '/' });
            GameObject.FindObjectOfType<Card>().rightQuote = goodAnswersRightQuote[1];
        }
    }

    IEnumerator GetTexture(string url) 
    {
        
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            GameObject.FindObjectOfType<GameManager>().cardSpriteRenderer.sprite = Sprite.Create((Texture2D) myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height),
                new Vector2(0.5f, 0.5f), 100.0f);
        }
        
    }
    public void NextText()
    {
        //Bool pour les facts
        GameObject.FindObjectOfType<FactDisplay>().check = true;

        //Affichage facts
        GameObject.FindObjectOfType<FactDisplay>().textDisplay.text = index[1];
    }

    //Suppression des phrases déjà passées
    public void DeleteSentence()
    {
        if (listData.Count > 1)
        {
            listData.RemoveAt(randomNmb);
        }
    }
}
