﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using GoogleSheetsToUnity;
using UnityEngine;

public class ListData : MonoBehaviour
{
    public static List<string> listData;
    public static List<string> listData2;
    public static List<string> listData3;
    private void Awake()
    {
        SpreadsheetManager.Read(new GSTU_Search("1l_MlEe-10F3GYJ3U7Oleqhpg0hwFYjZqaj413wQu9FE", "FeuilleInformation"), Test);
        
        DontDestroyOnLoad(gameObject);
    }

    void Test(GstuSpreadSheet spreadSheetRef)
    {
        //Debug.Log(spreadSheetRef["B4"].value);
        //On récupère le chemin du fichier
        //string pathTxt = Application.dataPath + "/DataBaseOnline.txt";
        //On créer un stringBuilder pour construire notre texte
        StringBuilder str = new StringBuilder();
        //Pour chaque ligne du tableau
        foreach (var row in spreadSheetRef.rows.primaryDictionary)
        {
            //Pour chaque cellule de la ligne
            foreach (var cell in row.Value)
            {
                //On ajoute le contenu de la cellule au texte + une barre
                str.Append(cell.value + "|");
            }
            //On enlève la dernière barre
            str.Remove(str.Length-1,1);
            //On ajoute un retour à la ligne
            str.Append("\n");
        }
        //On retire le dernier retour à la ligne
        str.Remove(str.Length-1,1);
        //On écrit tout dans le string
        string infoData = str.ToString();
        //on split le string dans un tableau de string
        listData = new List<string>(infoData.Split(new char[]{'\n'}));
        Debug.Log("c'est activé");
        
        listData.RemoveAt(0);
        SpreadsheetManager.Read(new GSTU_Search("1l_MlEe-10F3GYJ3U7Oleqhpg0hwFYjZqaj413wQu9FE", "FeuilleInformation2"), Test2);
    }

    void Test2(GstuSpreadSheet spreadSheetRef2)
    {
        StringBuilder str = new StringBuilder();
        //Pour chaque ligne du tableau
        foreach (var row in spreadSheetRef2.rows.primaryDictionary)
        {
            //Pour chaque cellule de la ligne
            foreach (var cell in row.Value)
            {
                //On ajoute le contenu de la cellule au texte + une barre
                str.Append(cell.value + "|");
            }
            //On enlève la dernière barre
            str.Remove(str.Length-1,1);
            //On ajoute un retour à la ligne
            str.Append("\n");
        }
        //On retire le dernier retour à la ligne
        str.Remove(str.Length-1,1);
        //On écrit tout dans le string
        string infoData = str.ToString();
        //on split le string dans un tableau de string
        listData2 = new List<string>(infoData.Split(new char[]{'\n'}));
        Debug.Log("c'est activé");
        listData2.RemoveAt(0);
        listData.AddRange(listData2);
        SpreadsheetManager.Read(new GSTU_Search("1l_MlEe-10F3GYJ3U7Oleqhpg0hwFYjZqaj413wQu9FE", "FeuilleInformation3"), Test3);
    }

    void Test3(GstuSpreadSheet spreadSheetRef3)
    {
        StringBuilder str = new StringBuilder();
        //Pour chaque ligne du tableau
        foreach (var row in spreadSheetRef3.rows.primaryDictionary)
        {
            //Pour chaque cellule de la ligne
            foreach (var cell in row.Value)
            {
                //On ajoute le contenu de la cellule au texte + une barre
                str.Append(cell.value + "|");
            }
            //On enlève la dernière barre
            str.Remove(str.Length-1,1);
            //On ajoute un retour à la ligne
            str.Append("\n");
        }
        //On retire le dernier retour à la ligne
        str.Remove(str.Length-1,1);
        //On écrit tout dans le string
        string infoData = str.ToString();
        //on split le string dans un tableau de string
        listData3 = new List<string>(infoData.Split(new char[]{'\n'}));
        Debug.Log("c'est activé");
        listData3.RemoveAt(0);
        listData.AddRange(listData3);
    }
}
