﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using UnityEngine;
using GoogleSheetsToUnity;
using UnityEngine.UI;
using UnityEngine.Networking;


public class liaison : MonoBehaviour
{
    public Image image;

    public string[] dataOnline;
    
    public List<string> listData;

    public string[] index;
    public string goodAnswers;
    public int randomNmb;
    // Start is called before the first frame update
    void Start()
    {
        SpreadsheetManager.Read(new GSTU_Search("1l_MlEe-10F3GYJ3U7Oleqhpg0hwFYjZqaj413wQu9FE", "FeuilleTest"), Test);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator GetTexture(string url) 
    {
        
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            image.sprite = Sprite.Create((Texture2D) myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height),
                new Vector2(0.5f, 0.5f), 100.0f);
        }
    }
    void Test(GstuSpreadSheet spreadSheetRef)
    {
        Debug.Log(spreadSheetRef["B4"].value);
        //On récupère le chemin du fichier
        //string pathTxt = Application.dataPath + "/DataBaseOnline.txt";
        //On créer un stringBuilder pour construire notre texte
        StringBuilder str = new StringBuilder();
        //Pour chaque ligne du tableau
        foreach (var row in spreadSheetRef.rows.primaryDictionary)
        {
            //Pour chaque cellule de la ligne
            foreach (var cell in row.Value)
            {
                //On ajoute le contenu de la cellule au texte + une barre
                str.Append(cell.value + "|");
            }
            //On enlève la dernière barre
            str.Remove(str.Length-1,1);
            //On ajoute un retour à la ligne
            str.Append("\n");
        }
        //On retire le dernier retour à la ligne
        str.Remove(str.Length-1,1);
        //On écrit tout dans le string
        string infoData = str.ToString();
        //on split le string dans un tableau de string
        dataOnline = infoData.Split(new char[] {'\n'});
        Debug.Log("c'est activé");
        
        listData = new System.Collections.Generic.List<string>(dataOnline);
        listData.RemoveAt(0);
        CalculateRandomNumber();
        
    }

    void CalculateRandomNumber()
    {
        //Phrase aléatoire
        randomNmb = Random.Range(0, listData.Count);

        //Séparation des colones 
        for (int i = 0; i < listData.Count; i++)
        {
            index = listData[randomNmb].Split(new char[] { '|' });
        }

        //Détection de la bonne réponse
        if (index[5].Contains("R/"))
        {
            goodAnswers = index[5];
        }
        else if (index[6].Contains("R/"))
        {
            goodAnswers = index[6];
        }
        StartCoroutine((GetTexture(index[7])));
    }
}

