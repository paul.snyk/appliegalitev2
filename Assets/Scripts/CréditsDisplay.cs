﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CréditsDisplay : MonoBehaviour
{
    //Chargement de la scène des crédits
    public void LoadCredit()
    {
        SceneManager.LoadScene(4);
    }

    //Déchargement de la scène de crédit
    public void UnLoadCredit()
    {
        SceneManager.LoadScene(0);
    }
}
