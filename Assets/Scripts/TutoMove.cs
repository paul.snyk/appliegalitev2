﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TutoMove : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOMoveX(3, 1));
        mySequence.Append( transform.DOMoveX(0, 1));
        mySequence.Append( transform.DOMoveX(-3, 1));
        mySequence.Append( transform.DOMoveX(0, 1));
        mySequence.SetLoops(-1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
