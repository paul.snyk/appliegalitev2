﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreBar : MonoBehaviour
{
    public Image barScore;
    static float score = 0.5f;

    public void Start()
    {
        barScore.fillAmount = score;
    }

    public void IncrementScore(float amount)
    {
        barScore.fillAmount += amount / 100;
    }

    public void Update()
    {
        if (barScore.fillAmount <= 0)
        {
            //Loose
            SceneManager.LoadScene(3);
        }
        else if (barScore.fillAmount >= 1 && !GameObject.FindObjectOfType<FactDisplay>().check)
        {
            //Win
            SceneManager.LoadScene(2);
        }

        if (GameObject.FindObjectOfType<DataBase>().listData.Count <= 1)
        {
            //Nouvelle pioche
            score = barScore.fillAmount;
            SceneManager.LoadScene(1);
        }
    }
}
