﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FactDisplay : MonoBehaviour
{
    public TextMeshPro textDisplay;
    public GameObject factGo;
    public GameObject cardGo;
    private GameManager gameManager;

    public bool check;

    private DataBase dataBase;

    public Animator anim;

    public GameObject panelFactGood;
    public GameObject panelFactBad;
    public GameObject panelErrorEffect;

    public bool falseAnswer = false;

    public AudioClip wrongSound;
    public AudioClip rightSound;

    public bool soundCheck = true;

    public void Start()
    {
        //Accès scripts
        dataBase = GameObject.FindObjectOfType<DataBase>();
        gameManager = GameObject.FindObjectOfType<GameManager>();

        //Initialisation bool
        check = false;
    }

    public void Update()
    {
        if (check)
        {
            gameManager.dragging = false;
            cardGo.SetActive(false);
            factGo.SetActive(true);

            anim.SetBool("BadAnswer", false);

            //Affichage fact
            textDisplay.text = dataBase.index[1];

            if (falseAnswer)
            {
                panelErrorEffect.SetActive(true);
                anim.SetBool("BadAnswer", true);
                panelFactBad.SetActive(true);
                panelFactGood.SetActive(false);
            }
            else
            {
                panelErrorEffect.SetActive(false);
                anim.SetBool("BadAnswer", false);
                panelFactGood.SetActive(true);
                panelFactBad.SetActive(false);
            }
        }
    }

    //Fin de boucle
    public void ReturnCard()
    {
        check = false;
        factGo.SetActive(false);
        anim.SetBool("BadAnswer", false);
        falseAnswer = false;

        factGo.transform.position = new Vector3(0, 0, -0.5f);

        dataBase.DeleteSentence();
        dataBase.CalculateRandomNumber();
    }

    public void Sound(bool answer)
    {
        if (soundCheck == true)
        {
            if (answer)
            {
                //Right Sound joué lors du bonne réponse
                AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.clip = rightSound;
                audioSource.Play();
                Destroy(audioSource, rightSound.length);
                Debug.Log("if 5 ");
                soundCheck = false;
            }
            else
            {
                //Wrong Sound joué lors du mauvaise réponse
                AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.clip = wrongSound;
                audioSource.Play();
                Destroy(audioSource, wrongSound.length);
                Debug.Log("else 6 ");
                soundCheck = false;
            }
        }
    }
}
