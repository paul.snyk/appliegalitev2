﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndControl : MonoBehaviour
{
    public Image imageBg;

    public MainMenuManager cardControl;

    void Update()
    {
        //Désactivation de la mécanique de swap
        if (imageBg.color.a == 0)
        {
            cardControl.enabled = true;
        }
        else
        {
            cardControl.enabled = false;
        }
    }
}
